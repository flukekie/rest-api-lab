const express = require("express");
const bodyparser = require("body-parser");

const app = express();
const port = 3333;

// const data = require("./mock");

var MongoClient = require("mongodb").MongoClient;
var mongo_url = "mongodb://localhost:27017/";
var mongo_database = "ead";
var mongo_collection = "books";

/* */

// init bodyparser
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

// root
app.get("/mongo", (req, res) => {
  res.send("Welcome to MongoDB");
});

// get all
app.get("/mongo/books", (req, res) => {
  MongoClient.connect(
    mongo_url,
    function(err, db) {
      if (err) throw err;
      var dbo = db.db(mongo_database);
      dbo
        .collection(mongo_collection)
        .find({}, { projection: { _id: 0 } })
        .toArray(function(err, result) {
          if (err) throw err;
          res.json(result);
          db.close();
        });
    }
  );
});

//get single from id
app.get("/mongo/books/:id", (req, res) => {
  MongoClient.connect(
    mongo_url,
    function(err, db) {
      if (err) throw err;
      var dbo = db.db(mongo_database);
      var query = {id:parseInt(req.params.id)};
      dbo
        .collection(mongo_collection)
        .find(query,{ projection: { _id: 0 } })
        .toArray(function(err, result) {
          if (err) throw err;
          res.json(result);
          db.close();
        });
    }
  );
});

// create an item
app.post("/mongo/books", (req, res) => {
  MongoClient.connect(
    mongo_url,
    function(err, db) {
      if (err) throw err;
      var dbo = db.db(mongo_database);
      var obj = req.body;
      dbo.collection(mongo_collection).insertOne(obj, function(err, res) {
        if (err) throw err;
        console.log("[MDB] document inserted");
        db.close();
      });
    }
  );
  res.status(201).json(req.body);
});

// update an item
app.put("/mongo/books/:id", (req, res) => {
  MongoClient.connect(
    mongo_url,
    function(err, db) {
      if (err) throw err;
      var dbo = db.db(mongo_database);
      var query = {id:parseInt(req.params.id)};
      var updateobj = {$set:req.body};
      dbo.collection(mongo_collection).updateOne(query,updateobj, function(err, res) {
        if (err) throw err;
        console.log("[MDB] document updated");
        db.close();
      });
    }
  );
  res.status(201).json(req.body);
});

// delete an item
app.delete("/mongo/books/:id", (req, res) => {
  MongoClient.connect(
    mongo_url,
    function(err, db) {
      if (err) throw err;
      var dbo = db.db(mongo_database);
      var query = {id:parseInt(req.params.id)};
      dbo.collection(mongo_collection).deleteOne(query, function(err, res) {
        if (err) throw err;
        console.log("[MDB] document deleted");
        db.close();
      });
    }
  );
  res.status(204).send();
});

//listen server
app.listen(port, () => {
  console.log(`[Server] Running server on port ${port}`);
});

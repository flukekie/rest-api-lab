# REST API assignment

There are 2 versions: Node.js and Python

Database used: MongoDB



## Node.JS

Use `server.mongo.js` file to run the server with MongoDB connection

Otherwise use `server.1.js` for mock data

Requirements:

​	`npm i`





## Python

Use `app.py` file to run the server

Requirements:

​	`pip install flask flask-pymongo flask-restful`


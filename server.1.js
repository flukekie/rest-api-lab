const express = require("express");
const bodyparser = require("body-parser");
const app = express();
const portnum = 3000;
const data = require("./data");

// init bodyparser
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

// root
app.get("/", (req, res) => {
  res.send("express.js");
});
// get all
app.get("/books", (req, res) => {
  res.json(data);
});
//get single
app.get("/books/:id", (req, res) => {
  res.json(data.find(item => item.id == req.params.id));
});

// post an item
app.post("/books", (req, res) => {
  data.push(req.body);
  res.status(201).json(req.body);
});

app.put("/books/:id", (req, res) => {
  const updateIndex = data.findIndex(item => item.id == req.params.id);
  res.json(Object.assign(data[updateIndex], req.body));
});

app.delete("/books/:id", (req, res) => {
  const deleteIndex = data.findIndex(item => item.id == req.params.id);
  data.splice(deleteIndex, 1);
  res.status(204).send();
});
//listen server
app.listen(portnum, () => {
  console.log("running server on port " + portnum);
});

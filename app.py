# mongo.py
# pip install flask flask-pymongo flask-restful


from flask import Flask, jsonify, request
from flask_pymongo import PyMongo
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

app.config['MONGO_DBNAME'] = 'ead'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/ead'

# Disable redirecting on POST method from /star to /star/
app.url_map.strict_slashes = False

mongo = PyMongo(app)
books = mongo.db.books

@app.route('/books', methods=['GET'])
def get_all_books():
    output = []
    for b in books.find():
        output.append({'id':b['id'],'name': b['name'], 'address': b['address']})
    return jsonify({'result': output})

@app.route('/books/<int:book_id>', methods=['GET'])
def get_one_book(book_id):
    output = []
    b = books.find_one({'id' : book_id})
    if b:
        output.append({'id':b['id'],'name': b['name'], 'address': b['address']})
    else:
        output = "Not found"
    
    return jsonify({'result': output})

@app.route('/books', methods=['POST'])
def add_book():

    _name = request.json['name']
    _address = request.json['address']
    _id = int(request.json['id'])

    newbook = books.insert({'name': _name, 'address': _address, 'id':_id})
    newbook_res = books.find_one({'_id': newbook })

    output = {'name' : newbook_res['name'], 'address' : newbook_res['address']}
    return jsonify({'result' : output})

@app.route('/books/<int:book_id>', methods=['DELETE'])
def delete_one_book(book_id):

    books.find_one_and_delete({'id' : book_id})
    
    return jsonify({'result': book_id})

@app.route('/books/<int:book_id>', methods=['PUT'])
def put_one_book(book_id):

    _name = request.json['name']
    _address = request.json['address']
    _id = int(request.json['id'])
    
    books.find_one_and_update({'id' : book_id},{'$set':{'name': _name, 'address': _address, 'id':_id}})

    return jsonify({'result' : book_id})


if __name__ == '__main__':
    app.run(debug=True)
